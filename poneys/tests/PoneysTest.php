<?php
use PHPUnit\Framework\TestCase;


require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $Poneys;
   

    public function test_removePoneyFromField() {
      // Setup
       $Poneys = new Poneys();

      // Action
     $Poneys->removePoneyFromField(3);

     //$Poneys->addPoneys(6);
      // Assert
      $this->assertEquals(5, $Poneys->getCount());
    }

     /**
     * @dataProvider resultProvider
     */

     public function test_removePoneyFromFieldProvider($number,$expected) {
      // Setup
      $Poneys = new Poneys();

      // Action
     $Poneys->removePoneyFromField($number);


      // Assert
      $this->assertEquals($expected, $Poneys->getCount());
    }


    public function resultProvider()
    {
        return [
            [3, 5],
            [2, 6],
            [1, 7],
            [0, 8]
        ];
    }


     /**
     * @dataProvider negativeProvider
     */

     public function test_removePoneyFromFieldNegativeProvider($number) {
      // Setup
      $Poneys = new Poneys();

      // Action
     
       $Poneys->removePoneyFromField($number); 
    }


    public function negativeProvider()
    {
        return [
            [0]
        ];
    }



    public function test_getNames(){
      $Poneys = new Poneys();
      $Poneys = $this->getMockBuilder('Poneys')
                         ->getMock();

      $Poneys->expects($this->exactly(1))
                 ->method('getNames')
                 ->willReturn(
                  array('safae','nawal','hanaa','adnane')
                 ); 
      $this->assertEquals(
                  array('safae','nawal','hanaa','adnane'),
                  $Poneys->getNames()

      );        

    }

    public function setUp()
    {

               $Poneys = new Poneys();
               $Poneys->setCount(7);

    }

    public function testDisponibilite(){
      $Poneys= new Poneys();
      $Poneys->addPoneys(2);
      $this->assertEquals(true,$Poneys->dispoPoneys());

    }




  }
 ?>

